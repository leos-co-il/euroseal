<?php
get_header();
$fields = get_fields();
?>
<div class="post-output-block">
	<div class="container pt-5">
		<?php
		$s = get_search_query();
		$args = array(
			's' =>$s
		);
		$the_query = new WP_Query( $args );
		if ( $the_query->have_posts() ) { ?>
		<h4 class="base-title-blue my-5"><?= esc_html__('תוצאות חיפוש עבור:','leos');?><?= get_query_var('s') ?></h4>
		<div class="row justify-content-center">
			<?php  while ( $the_query->have_posts() ) { $the_query->the_post(); $hey = 1; ?>
				<div class="col-xl-4 col-md-6 col-12 mb-4 wow fadeInUp" data-wow-delay="0.<?= $hey * 2; ?>s">
					<div class="post-item">
						<?php $type = get_post_type(); ?>
						<div class="post-image"
							<?php if (has_post_thumbnail()) : ?>
								style="background-image: url('<?= postThumb(); ?>')"
							<?php endif;?>>
						</div>
						<div class="post-item-content post-card-content">
							<h3 class="post-item-title"><?php the_title(); ?></h3>
							<p class="base-text text-center"><?= text_preview(get_the_content(), '15'); ?></p>
						</div>
						<a href="<?php the_permalink(); ?>" class="base-link base-link-blue mb-3 post-link">
							המשך למאמר
						</a>
					</div>
				</div>
			<?php }
			} else{ ?>
				<div class="text-center pt-5">
					<h4 class="base-block-title text-center">
						<?= esc_html__('שום דבר לא נמצא','leos'); ?>
					</h4>
				</div>
				<div class="alert alert-info text-center mt-5">
					<p><?= esc_html__('מצטערים, אך שום דבר לא תאם את קריטריוני החיפוש שלך. אנא נסה שוב עם מילות מפתח שונות.','leos'); ?></p>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
