<?php

$tel = opt('tel');
$mail = opt('mail');
$fax = opt('fax');
$address = opt('address');
$open_hours = opt('open_hours');
$current_id = get_the_ID();
$contact_id = getPageByTemplate('views/contact.php');
$logo = opt('logo');
$map = opt('map_image');

?>

<footer>
	<div class="footer-main">
		<?php if ($current_id !== $contact_id) : ?>
			<div class="foo-form-wrap">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-xl-9 col-lg-10 col-md-11 col-12">
							<div class="form-col">
								<?php if ($f_title = opt('foo_form_title')) : ?>
									<h2 class="base-title-blue text-center"><?= $f_title; ?></h2>
								<?php endif;
								if ($f_text = opt('foo_form_subtitle')) : ?>
									<h3 class="base-text text-center mb-3"><?= $f_text; ?></h3>
								<?php endif;
								getForm('26'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<a id="go-top">
			<div class="go-top-wrap">
				<h5 class="to-top-text">חזרה למעלה</h5>
			</div>
		</a>
		<div class="container">
			<div class="row justify-content-between">
				<div class="col-xl-3 col-lg-auto col-12 foo-menu main-footer-menu">
					<h3 class="foo-title">תפריט ניווט באתר //</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-menu', '2', 'hop-hey',
								'main_menu h-100 text-right'); ?>
					</div>
				</div>
				<div class="col-lg col-12 foo-menu links-footer-menu">
					<h3 class="foo-title">מאמרים רלוונטיים //</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-links-menu', '2', 'hop-hey three-columns'); ?>
					</div>
				</div>
				<div class="col-lg-3 col-12 foo-menu contacts-footer-menu">
					<h3 class="foo-title">צור קשר //</h3>
					<div class="menu-border-top contact-menu-foo">
						<ul class="contact-list d-flex flex-column">
							<?php if ($tel) : ?>
								<li>
									<a href="tel:<?= $tel; ?>" class="contact-info-footer">
										<img src="<?= ICONS ?>form-tel.png">
										<span><?= 'טלפון: '.$tel; ?></span>
									</a>
								</li>
							<?php endif; ?>
							<?php if ($mail) : ?>
								<li>
									<a href="mailto:<?= $mail; ?>" class="contact-info-footer">
										<img src="<?= ICONS ?>form-mail.png">
										<span><?= 'מייל: '.$mail; ?></span>
									</a>
								</li>
							<?php endif; ?>
							<?php if ($address) : ?>
								<li>
									<a href="https://waze.com/ul?q=<?= $address; ?>"
									   class="contact-info-footer" target="_blank">
										<img src="<?= ICONS ?>geo-small.png">
										<span><?= 'כתובת: '.$address; ?></span>
									</a>
								</li>
							<?php endif; ?>
						</ul>
						<?php if ($map) : ?>
							<div class="map-footer">
								<img src="<?= $map['url']; ?>">
							</div>
						<?php endif; ?>
						<?php if ($logo) : ?>
							<a href="/" class="logo logo-footer align-self-end">
								<img src="<?= $logo['url'] ?>" alt="logo-euroseal">
							</a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>

<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
    require_once THEMEPATH . "/inc/debug.php"
    ?>
    <script>

        function _fetchHeader($_el){
            let res = {
                'count' : 0,
                'content' : ''
            } ;
            $($_el).each(function () {
                res.count++;
                res.content += ' [' + $(this).text() + '] ';
            });
            return 'Count: ' + res.count + '. Text: ' + res.content;
        }

        function _fetchMeta($_meta){
            return $('meta[name='+$_meta+']').attr("content");
        }




        phpdebugbar.addDataSet({
            "SEO Local": {
                'H1' : _fetchHeader('h1'),
                'H2' : _fetchHeader('h2'),
                'H3' : _fetchHeader('h3'),
                'Meta Title' : _fetchMeta('title'),
                'Meta Description' : _fetchMeta('description'),
                'Meta Keywords' : _fetchMeta('keywords'),
            }
        });
    </script>

<?php endif; ?>

</body>
</html>
