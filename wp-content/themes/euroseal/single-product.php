<?php

the_post();
get_header();
$fields = get_fields();
$postId = get_the_ID();
$post_terms = wp_get_object_terms($postId, 'product_cat', ['fields' => 'ids']);
$sameProducts = [];
if ($fields['same_products']) {
	$sameProducts = $fields['same_products'];
} else {
	$sameProducts = get_posts([
		'posts_per_page' => 4,
		'post_type' => 'product',
		'post__not_in' => array($postId),
		'tax_query' => [
			[
				'taxonomy' => 'product_cat',
				'field' => 'term_id',
				'terms' => $post_terms,
			],
		],
	]);
}
if ($sameProducts === NULL) {
	$sameProducts = get_posts([
		'posts_per_page' => 4,
		'orderby' => 'rand',
		'post_type' => 'product',
		'post__not_in' => array($postId),
	]);
}
?>

<article class="page-body">
	<?php get_template_part('views/partials/content', 'block_top', [
		'title' => get_the_title(),
		'back_img' => has_post_thumbnail() ? postThumb() : '',
	]); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-10 col-md-11 col-12">
				<?php if ( function_exists('yoast_breadcrumb') ) : ?>
					<div class="row justify-content-start">
						<div class="col-12 breadcol">
							<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
		<div class="row justify-content-between align-items-start my-5">
			<div class="col-xl-7 col-lg-8 col-12">
				<div class="block-text block-text-single product-page-content">
					<?php the_content(); ?>
				</div>
				<?php if ($fields['product_info']) : ?>
					<div id="accordion-product" class="product-info-acc">
						<?php foreach ($fields['product_info'] as $number => $info_item) : ?>
							<div class="card">
								<div class="prod-desc-header" id="heading_<?= $number; ?>">
									<div class="accordion-button-wrap">
										<h2 class="product-info-title"><?= $info_item['prod_info_title']; ?></h2>
										<button class="btn" data-toggle="collapse"
												data-target="#contactInfo<?= $number; ?>"
												aria-expanded="false" aria-controls="collapseOne">
											<span class="accordion-symbol plus-icon
												<?php echo $number === 0 ? 'hide-icon' : ''; ?>">
												<img src="<?= ICONS ?>plus-prod.png">
											</span>
											<span class="accordion-symbol minus-icon
												<?= $number === 0 ? 'show-icon' : ''; ?>">
												<img src="<?= ICONS ?>minus-prod.png">
											</span>
										</button>
									</div>
									<div id="contactInfo<?= $number; ?>" class="collapse
											<?php echo $number === 0 ? 'show' : ''; ?>"
										 aria-labelledby="heading_<?= $number; ?>" data-parent="#accordion-product">
										<div class="block-text block-text-single product-page-content">
											<?= $info_item['prod_info_text']; ?>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-lg-4 col-12 post-images-col product-img-col">
				<?php if (has_post_thumbnail()) : ?>
					<img src="<?= postThumb(); ?>">
				<?php endif;
				if ($fields['product_files']) : ?>
					<div class="d-flex flex-column justify-content-start">
						<?php if ($fields['prod_files_title']) : ?>
							<h3 class="files-title"><?= $fields['prod_files_title']; ?></h3>
						<?php endif; ?>
						<div class="files-line">
							<?php foreach ($fields['product_files'] as $n => $file) : ?>
								<a href="<?= isset($file['file']['url']) ? $file['file']['url'] : ''; ?>" class="file-item"
								   target="_blank">
								</a>
							<?php endforeach; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
		<?php if ($fields['post_gallery']) : ?>
			<div class="row justify-content-center">
				<div class="col">
					<h2 class="base-title-blue gallery-title">גלרית מוצר</h2>
				</div>
			</div>
			<div class="row justify-content-center gallery-row mb-5">
				<?php foreach ($fields['post_gallery'] as $img) : ?>
					<div class="col-sm-6 col-12 gallery-image-col">
						<a href="<?= $img['url']; ?>" style="background-image: url('<?= $img['url'];?>')"
						class="gallery-image-prod" data-lightbox="product-images"></a>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
</article>



<?php get_template_part('views/partials/repeat', 'form');
if ($sameProducts) : ?>
	<div class="block-part">
		<div class="container">
			<div class="row justify-content-center">
				<?php if ($fields['same_prod_text']) {
					get_template_part('views/partials/content', 'block_desc', [
						'text' => $fields['same_prod_text'] ? $fields['same_prod_text'] : '<h2>מוצרים נודפים</h2>',
					]);}
				?>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($sameProducts as $x => $product) : ?>
					<div class="col-xl col-md-4 col-sm-6 col-12 post-col post-col-home wow fadeIn" data-wow-delay="0.<?= $x; ?>s">
						<?php get_template_part('views/partials/card', 'product_home', [
								'post' => $product,
						]); ?>
					</div>
				<?php endforeach; ?>
			</div>
			<?php if ($fields['same_prod_link'] || $link = opt('products_page')) {
				get_template_part('views/partials/content', 'block_link', [
					'link' => $fields['same_prod_link'] ? $fields['same_prod_link'] : $link,
				]);
			}?>
		</div>
	</div>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>
