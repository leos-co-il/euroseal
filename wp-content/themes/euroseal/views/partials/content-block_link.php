<?php if (isset($args['link']) && $args['link']) : ?>
<div class="row justify-content-end my-4 link-row">
	<div class="col-auto">
		<a href="<?= isset($args['link']['url']) ? $args['link']['url'] : ''; ?>" class="base-link base-link-blue">
			<?= isset($args['link']['title']) ? $args['link']['title'] : 'קרא עוד'; ?>
		</a>
	</div>
</div>
<?php endif; ?>
