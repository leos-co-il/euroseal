<?php if (isset($args['text']) && $args['text']) : ?>
	<div class="col-xl-6 col-lg8 col-md-10 col-12 mb-4">
		<div class="block-text text-center">
			<?= $args['text']; ?>
		</div>
	</div>
<?php endif; ?>
