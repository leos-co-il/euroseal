<div class="top-page" <?php if (isset($args['back_img']) && $args['back_img']) : ?>
	style="background-image: url('<?= $args['back_img']; ?>')"
<?php endif; ?>>
	<div class="top-overlay">
		<div class="container h-100">
			<div class="row h-100 justify-content-center align-items-end">
				<div class="col">
					<?php if (isset($args['title']) && $args['title']) : ?>
						<h2 class="base-title-blue text-center"><?= $args['title']; ?></h2>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>
