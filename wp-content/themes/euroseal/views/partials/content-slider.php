<?php if (isset($args['content']) && $args['content']) : ?>
	<section class="slider-base-wrap arrows-slider">
		<div class="container slider-container">
			<div class="row justify-content-center align-items-center slider-row">
				<div class="col-xl-6 col-lg-8 col-12 <?= (isset($args['img']) && $args['img']) ? '': 'col-lg-12'; ?> slider-wrap-col">
					<div class="slider-text-wrap">
						<div class="base-slider" dir="rtl">
							<?php foreach ($args['content'] as $content) : ?>
								<div>
									<div class="block-text"><?= $content['content']; ?></div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
				<?php if (isset($args['img']) && $args['img']) : ?>
					<div class="col-xl-6 col-lg-4 col-12 d-flex justify-content-center align-items-center slider-img-col">
						<img src="<?= $args['img']['url']; ?>" class="slider-img">
						<div class="slider-blue-part"></div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php endif; ?>
