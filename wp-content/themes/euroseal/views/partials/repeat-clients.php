<?php if ($clients = opt('clients')) : ?>
	<div class="clients-block arrows-slider">
		<div class="container h-100">
			<?php if ($partners_title = opt('clients_block_title')) : ?>
				<div class="row justify-content-center">
					<div class="col">
						<h2 class="base-title-blue mb-3"><?= $partners_title; ?></h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center h-100 clients-line">
				<div class="col-12 d-flex justify-content-center align-items-center h-100">
					<div class="clients-slider" dir="rtl">
						<?php foreach ($clients as $client) : ?>
							<div class="client-logo">
								<img src="<?= $client['url']; ?>" alt="client-logo">
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
