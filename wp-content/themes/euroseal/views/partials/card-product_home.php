<?php if(isset($args['post']) && $args['post']) :
	$link = get_the_permalink($args['post']); ?>
	<div class="post-item product-item">
		<a class="post-image product-item-image" <?php if (has_post_thumbnail($args['post'])) : ?>
			style="background-image: url('<?= postThumb($args['post']); ?>')" <?php endif; ?>
		   href="<?= $link; ?>"></a>
		<div class="post-item-content">
			<a class="post-item-title product-item-title" href="<?= $link; ?>"><?= $args['post']->post_title; ?></a>
			<p class="base-text text-center mb-3">
				<?= text_preview($args['post']->post_content, 10); ?>
			</p>
		</div>
		<a href="<?= $link; ?>" class="base-link">
			עבור למוצר
		</a>
	</div>
<?php endif; ?>
