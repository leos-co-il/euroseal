<?php if ($logo = opt('logo')) : ?>
	<a class="logo" href="/">
		<img src="<?= $logo['url']; ?>" alt="euroseal-logo">
	</a>
<?php endif; ?>
