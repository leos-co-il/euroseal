<?php if ($benefits = opt('benefit_item')) : ?>
	<section class="benefits-block">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-9 col-md-11 col-12">
					<?php if ($title = opt('benefits_block_title')) : ?>
						<div class="row justify-content-center">
							<div class="col mb-4">
								<h2 class="base-title-orange"><?= $title; ?></h2>
							</div>
						</div>
					<?php endif; ?>
					<div class="row justify-content-center align-items-start">
						<?php foreach ($benefits as $key => $benefit) : ?>
							<div class="col-lg-3 col-sm-6 col-12 benefit-col wow flipInY" data-wow-delay="0.<?= $key * 2; ?>s">
								<div class="benefit-item">
									<div class="benefit-icon-wrap">
										<?php if ($benefit['benefit_icon']) : ?>
											<img src="<?= $benefit['benefit_icon']['url']; ?>" alt="benefit-icon">
										<?php endif; ?>
									</div>
									<p class="base-text benefit-text">
										<?= $benefit['benefit_text']; ?>
									</p>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
