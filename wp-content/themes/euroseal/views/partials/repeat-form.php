<div class="repeat-form-wrap">
	<div class="container wow zoomIn">
		<div class="row justify-content-center mb-3">
			<div class="col-xl-2 col-lg-3 col-md-4 col-sm-5 col-6">
				<?php get_template_part('views/partials/repeat', 'logo'); ?>
			</div>
		</div>
		<div class="row align-items-center justify-content-center">
			<div class="col-xl-7 col-lg-8 col-md-10 col-12 form-col">
				<?php if ($f_title = opt('mid_form_title')) : ?>
					<h2 class="base-title-black"><?= $f_title; ?></h2>
				<?php endif;
				if ($f_text = opt('mid_form_text')) : ?>
					<h3 class="mid-form-text text-center"><?= $f_text; ?></h3>
				<?php endif; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<?php getForm('27'); ?>
			</div>
		</div>
	</div>
</div>
