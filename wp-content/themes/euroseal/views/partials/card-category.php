<?php if(isset($args['category']) && $args['category']) :
	$link = get_term_link($args['category']); ?>
	<div class="base-product-item">
		<div class="product-item-inside">
			<a class="post-image cat-item-image" <?php if ($img = get_field('cat_img', $args['category'])) : ?>
				style="background-image: url('<?= $img['url']; ?>')" <?php endif; ?>
			   href="<?= $link; ?>"></a>
			<div class="post-item-content">
				<a class="cat-item-title" href="<?= $link; ?>"><?= $args['category']->name; ?></a>
				<p class="cat-item-desc" href="<?= $link; ?>">
					<?= text_preview(category_description($args['category']), 15); ?>
				</p>
			</div>
			<a href="<?= $link; ?>" class="base-link">
				עבור לקטגוריה
			</a>
		</div>
	</div>
<?php endif; ?>
