<?php if(isset($args['post']) && $args['post']) :
	$link = get_the_permalink($args['post']); ?>
	<div class="base-product-wrapper">
		<div class="base-product-item">
			<div class="product-item-inside">
				<a class="post-image single-product-item-image" <?php if (has_post_thumbnail($args['post'])) : ?>
					style="background-image: url('<?= postThumb($args['post']); ?>')" <?php endif; ?>
				   href="<?= $link; ?>"></a>
				<div class="post-item-content">
					<a class="base-prod-item-title" href="<?= $link; ?>"><?= $args['post']->post_title; ?></a>
				</div>
				<a href="<?= $link; ?>" class="base-link">
					עבור למוצר
				</a>
			</div>
		</div>
	</div>
<?php endif; ?>
