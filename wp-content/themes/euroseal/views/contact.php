<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

$tel = opt('tel');
$mail = opt('mail');
$address = opt('address');
$open_hours = opt('open_hours');
?>

<article class="page-body">
	<?php get_template_part('views/partials/content', 'block_top', [
		'title' => get_the_title(),
		'back_img' => has_post_thumbnail() ? postThumb() : '',
	]); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-10 col-md-11 col-12">
				<?php if ( function_exists('yoast_breadcrumb') ) : ?>
					<div class="row justify-content-start">
						<div class="col-12 breadcol">
							<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
						</div>
					</div>
				<?php endif; ?>
				<div class="row justify-content-center">
					<?php if ($tel) : ?>
						<a href="tel:<?= $tel; ?>" class="col-lg-3 col-sm-6 col-11 contact-item contact-item-link wow zoomIn"
						   data-wow-delay="0.2s">
							<div class="contact-icon-wrap">
								<img src="<?= ICONS ?>contact-tel.png">
							</div>
							<h3 class="contact-info-title">
								טלפון:
							</h3>
							<p class="base-text text-center">
								<?= $tel; ?>
							</p>
						</a>
					<?php endif;
					if ($mail) : ?>
						<a href="mailto:<?= $mail; ?>" class="contact-item col-lg-3 col-sm-6 col-11 contact-item-link wow zoomIn"
						   data-wow-delay="0.4s">
							<div class="contact-icon-wrap">
								<img src="<?= ICONS ?>contact-mail.png">
							</div>
							<h3 class="contact-info-title">
								מייל:
							</h3>
							<p class="base-text text-center">
								<?= $mail; ?>
							</p>
						</a>
					<?php endif;
					if ($address) : ?>
						<a href="https://www.waze.com/ul?q=<?= $address; ?>"
						   class="contact-item-link contact-item col-lg-3 col-sm-6 col-11 wow zoomIn" data-wow-delay="0.6s">
							<div class="contact-icon-wrap">
								<img src="<?= ICONS ?>contact-geo.png">
							</div>
							<h3 class="contact-info-title">
								מיקום:
							</h3>
							<p class="base-text text-center">
								<?= $address; ?>
							</p>
						</a>
					<?php endif;
					if ($open_hours) : ?>
						<div class="contact-item col-lg-3 col-sm-6 col-11 wow zoomIn" data-wow-delay="0.8s">
							<div class="contact-icon-wrap">
								<img src="<?= ICONS ?>contact-hours.png">
							</div>
							<h3 class="contact-info-title">
								שעות פעילות
							</h3>
							<div class="base-text text-center">
								<?= $open_hours; ?>
							</div>
						</div>
					<?php endif; ?>
				</div>
				<div class="row justify-content-center my-5">
					<div class="col contact-form-col">
						<div class="form-col">
							<?php if ($fields['contact_form_title']) : ?>
								<h3 class="base-title-black mb-2"><?= $fields['contact_form_title']; ?></h3>
							<?php endif;
							if ($fields['contact_form_text']) : ?>
								<p class="base-text text-center mb-3"><?= $fields['contact_form_text']; ?></p>
							<?php endif;
							getForm('28'); ?>
						</div>
					</div>
					<?php if ($map = opt('map_frame')) : ?>
						<div class="col-lg-6 col-12 map-col">
							<?= $map; ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
