<?php
/*
Template Name: מאמרים
*/
get_header();
$fields = get_fields();
$posts = get_posts([
	'numberposts' => -1,
	'post_type' => 'post',
]);
?>
<article class="page-body">
	<?php get_template_part('views/partials/content', 'block_top', [
		'title' => get_the_title(),
		'back_img' => has_post_thumbnail() ? postThumb() : '',
	]); ?>
	<div class="body-output">
		<div class="container">
			<?php if ( function_exists('yoast_breadcrumb') ) : ?>
				<div class="row justify-content-start">
					<div class="col-12 breadcol">
						<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
					</div>
				</div>
			<?php endif; ?>
		</div>
		<?php if ($posts) : $counter = 1; ?>
			<div class="container">
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($posts as $x => $post) : ?>
						<div class="col-xl-3 col-lg-4 col-sm-6 col-12 post-col wow fadeInUp" data-wow-delay="0.<?= $x * 2; ?>s">
							<?php get_template_part('views/partials/card', 'post', [
									'post' => $post,
							]); ?>
						</div>
					<?php if(($counter % 8 === 0) || ($counter === count($posts) && $counter <= 8)) : ?>
				</div>
			</div>
			<?php get_template_part('views/partials/repeat', 'form'); ?>
		<div class="container pt-4">
			<div class="row justify-content-center align-items-stretch">
				<?php endif; $counter++; endforeach; ?>
			</div>
		</div>
	</div>
	<?php endif; ?>
</article>
<?php if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>

