<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();

?>

<?php if ($fields['main_slider']) : ?>
	<section class="home-main-block">
		<div class="main-slider" dir="rtl">
			<?php foreach ($fields['main_slider'] as $slide) : ?>
				<div class="slide-home" <?php if ($slide['home_main_img']): ?>
					style="background-image: url('<?= $slide['home_main_img']['url']; ?>')"
				<?php endif; ?>>
					<div class="main-slide-item">
						<div class="container">
							<div class="row">
								<div class="col-xl-6 col-lg-7 col-md-9 col-12">
									<?php if ($slide['home_main_text']) : ?>
										<div class="block-text-home">
											<?= $slide['home_main_text']; ?>
										</div>
									<?php endif; ?>
								</div>
							</div>
							<?php if ($slide['home_main_link']) {
								get_template_part('views/partials/content', 'block_link', [
										'link' => $slide['home_main_link'],
										]);
							} ?>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</section>
<?php endif;
if ($fields['home_about_text'] || $fields['home_about_img']) : ?>
<section class="about-home">
	<div class="container">
		<div class="row justify-content-center align-items-center">
			<?php if ($fields['home_about_img']) : ?>
				<div class="<?= $fields['home_about_text'] ? 'col-lg-5' : 'col-lg-12'; ?> col-12 about-img-col">
					<img src="<?= $fields['home_about_img']['url']; ?>" alt="about-img" class="about-img">
					<div class="absolute-year">
						<img src="<?= IMG ?>year-img.png" alt="year">
					</div>
				</div>
			<?php endif;
			if ($fields['home_about_text']) : ?>
				<div class="<?= $fields['home_about_img'] ? 'col-lg-7' : 'col-lg-12'; ?> col-12 about-content-col">
					<div class="block-text">
						<?= $fields['home_about_text']; ?>
					</div>
					<?php if ($fields['home_about_link']) {
						get_template_part('views/partials/content', 'block_link', [
								'link' => $fields['home_about_link'],
						]);
					}
					if ($logo = opt('logo')) : ?>
						<a href="/" class="logo-about">
							<img src="<?= $logo['url'] ?>" alt="logo-euroseal">
						</a>
					<?php endif; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php endif;
get_template_part('views/partials/repeat', 'benefits');
if ($fields['home_cats_block_text'] || $fields['home_cats']) : ?>
	<section class="block-part">
		<div class="container">
			<div class="row justify-content-center">
				<?php if ($fields['home_cats_block_text']) {
						get_template_part('views/partials/content', 'block_desc', [
								'text' => $fields['home_cats_block_text'],
						]);}
				?>
			</div>
			<?php if ($fields['home_cats']) : ?>
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($fields['home_cats'] as $x => $cat) : ?>
						<div class="col-xl-3 col-lg-4 col-sm-6 col-12 post-col wow fadeIn" data-wow-delay="0.<?= $x; ?>s">
							<?php get_template_part('views/partials/card', 'category', [
									'category' => $cat,
							]); ?>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
get_template_part('views/partials/repeat', 'clients');
get_template_part('views/partials/repeat', 'form');
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
if ($fields['h_prod_slider'] || $fields['h_prod_block_text']) : ?>
	<section class="block-part">
		<div class="container">
			<div class="row justify-content-center">
				<?php if ($fields['h_prod_block_text']) {
					get_template_part('views/partials/content', 'block_desc', [
							'text' => $fields['h_prod_block_text'],
					]);}
				?>
			</div>
			<?php if ($fields['h_prod_slider']) : ?>
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($fields['h_prod_slider'] as $x => $product) : ?>
						<div class="col-xl col-md-4 col-sm-6 col-12 post-col post-col-home wow fadeIn" data-wow-delay="0.<?= $x; ?>s">
							<?php get_template_part('views/partials/card', 'product_home', [
									'post' => $product,
							]); ?>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif;
			if ($fields['h_prod_link']) {
				get_template_part('views/partials/content', 'block_link', [
						'link' => $fields['h_prod_link'],
				]);
			}?>
		</div>
	</section>
<?php endif;
if ($fields['reviews']) : ?>
	<!--Reviews-->
	<section class="block-part reviews">
		<div class="container">
			<div class="row justify-content-center mb-4">
				<?php if ($fields['reviews_block_title']) {
					get_template_part('views/partials/content', 'block_desc', [
							'text' => $fields['reviews_block_title'],
					]);}
				?>
			</div>
			<div class="row justify-content-center arrows-slider reviews-arrows">
				<div class="col-xl-10 col-reviews col-12">
					<div class="reviews-slider" dir="rtl">
						<?php foreach ($fields['reviews'] as $number => $revItem) : ?>
							<div class="review-slide">
								<div class="review-item">
									<div class="rev-pop-trigger">
										<div class="hidden-review">
											<div class="block-output">
												<?= $revItem['rev_text']; ?>
											</div>
										</div>
									</div>
									<?php if ($revItem['rev_img']) : ?>
										<div class="review-image-wrap">
											<div class="review-img">
												<img src="<?= $revItem['rev_img']['url']; ?>">
											</div>
										</div>
									<?php endif; ?>
									<div class="rev-content">
										<div class="review-prev">
											<?= text_preview($revItem['rev_text'], '20'); ?>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--Reviews pop-up-->
	<div class="modal fade" id="reviewsModal" tabindex="-1" role="dialog"
		 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
				<div class="modal-body" id="reviews-pop-wrapper"></div>
			</div>
		</div>
	</div>
<?php endif;
if ($fields['h_posts'] || $fields['h_posts_block_text']) : ?>
	<section class="block-part">
		<div class="container">
			<div class="row justify-content-center">
				<?php if ($fields['h_posts_block_text']) {
					get_template_part('views/partials/content', 'block_desc', [
							'text' => $fields['h_posts_block_text'],
					]);}
				?>
			</div>
			<?php if ($fields['h_posts']) : ?>
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($fields['h_posts'] as $x => $post) : ?>
						<div class="col-xl-3 col-sm-6 col-12 post-col wow fadeInUp" data-wow-delay="0.<?= $x * 2; ?>s">
							<?php get_template_part('views/partials/card', 'post', [
									'post' => $post,
							]); ?>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif;
			if ($fields['h_posts_link']) {
				get_template_part('views/partials/content', 'block_link', [
						'link' => $fields['h_posts_link'],
				]);
			}?>
		</div>
	</section>
<?php endif;
if ($fields['single_slider_seo']) : ?>
	<div class="slider-reverse">
		<?php get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
		]); ?>
	</div>
<?php endif;
get_footer(); ?>
