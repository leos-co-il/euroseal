<?php
/*
Template Name: אודות
*/
get_header();
$fields = get_fields();
?>
<article class="page-body about-page-body">
	<?php get_template_part('views/partials/content', 'block_top', [
			'title' => get_the_title(),
			'back_img' => has_post_thumbnail() ? postThumb() : '',
	]); ?>
	<div class="container">
		<?php if ( function_exists('yoast_breadcrumb') ) : ?>
			<div class="row justify-content-start">
				<div class="col-12 breadcol">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
				</div>
			</div>
		<?php endif; ?>
		<div class="row justify-content-center pt-4">
			<div class="col-xl-9 col-lg-10 col-12">
				<div class="block-text text-center about-text-ouput">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ($fields['about_video_link']) : ?>
			<div class="row justify-content-center">
				<div class="col-xl-5 col-lg-7 col-md-8 col-sm-10 col-12">
					<div class="video-item" style="background-image: url('<?= getYoutubeThumb($fields['about_video_link']); ?>')">
						<span class="play-video play-button" data-video="<?= getYoutubeId($fields['about_video_link']); ?>">
							<img src="<?= ICONS ?>play-button.png">
						</span>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<div class="video-modal">
	<div class="modal fade" id="modalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
		 aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-body" id="iframe-wrapper"></div>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" class="close-icon">×</span>
				</button>
			</div>
		</div>
	</div>
</div>
<?php
get_template_part('views/partials/repeat', 'benefits');
get_template_part('views/partials/repeat', 'clients');
get_template_part('views/partials/repeat', 'form');
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>

