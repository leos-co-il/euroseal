<?php
/*
Template Name: מוצרים
*/
get_header();
$fields = get_fields();
$products = get_posts([
	'numberposts' => -1,
	'post_type' => 'product',
]);
?>
<article class="page-body">
	<?php get_template_part('views/partials/content', 'block_top', [
			'title' => get_the_title(),
			'back_img' => has_post_thumbnail() ? postThumb() : '',
	]); ?>
	<div class="body-output">
		<div class="container">
			<?php if ( function_exists('yoast_breadcrumb') ) : ?>
				<div class="row justify-content-start">
					<div class="col-12 breadcol">
						<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
					</div>
				</div>
			<?php endif;
			if ($products) : ?>
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($products as $x => $product) : ?>
						<div class="col-xl-3 col-lg-4 col-sm-6 col-12 post-col wow fadeInUp" data-wow-delay="0.<?= $x * 2; ?>s">
							<?php get_template_part('views/partials/card', 'product', [
									'post' => $product,
							]); ?>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>
<?php
get_template_part('views/partials/repeat', 'form');
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>


