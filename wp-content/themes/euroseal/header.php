<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif; ?>


<header>
    <div class="container-fluid">
        <div class="row justify-content-center">
			<div class="col-xl-11 col-12 col-header">
				<div class="row">
					<div class="col-md-2 col-5 d-flex justify-content-start align-items-center">
						<div class="logo">
							<a href="/">
								<img src="<?= opt('logo_header')['url'] ?>" alt="logo-euroseal">
							</a>
						</div>
					</div>
					<div class="col d-flex align-items-center">
						<nav id="MainNav" class="h-100">
							<div id="MobNavBtn">
								<span></span>
								<span></span>
								<span></span>
							</div>
							<?php getMenu('header-menu', '2', '', 'main_menu h-100'); ?>
						</nav>
						<div class="wrap-search">
							<?= get_search_form() ?>
						</div>
					</div>
					<?php if ($tel = opt('tel')) : ?>
						<div class="col-md-auto col-3">
							<a href="tel:<?= $tel; ?>" class="header-tel d-flex justify-content-center align-items-center wow fadeInDown" data-wow-delay="0.2s">
								<div class="d-flex flex-column justify-content-center align-items-center">
									<?php if ($tel_text = opt('tel_text')) : ?>
										<span class="tel-header-text mb-2">
									<?= $tel_text; ?>
								</span>
									<?php endif; ?>
									<span class="d-flex align-items-center justify-content-center">
							<span class="tel-number">
								<?= $tel; ?>
							</span>
								<img src="<?= ICONS ?>header-tel.png" class="header-tel-icon">
						</span>
								</div>
							</a>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
    </div>
</header>
<div class="pop-trigger">
	<img src="<?= ICONS ?>pop-trigger.png" alt="open-popup">
</div>
<div class="menu-trigger">
	<img src="<?= ICONS ?>menu-trigger.png" alt="open-menu">
	<span class="side-title">מוצרים</span>
</div>
<section class="pop-form">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-6 col-lg-8 col-md-11 col-12 d-flex justify-content-center">
				<div class="float-form">
					<div class="bordered-form">
						<span class="close-form">
							<img src="<?= ICONS ?>close.png">
						</span>
						<div class="row align-items-center justify-content-center">
							<div class="col-12 form-col">
								<?php if ($f_title = opt('pop_form_title')) : ?>
									<h2 class="base-title-black"><?= $f_title; ?></h2>
								<?php endif;
								if ($f_text = opt('pop_form_subtitle')) : ?>
									<h3 class="mid-form-text text-center"><?= $f_text; ?></h3>
								<?php endif; ?>
							</div>
						</div>
						<div class="row justify-content-center">
							<div class="col-md-10 col-11">
								<?php getForm('26'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="side-menu-back">
	<div class="side-menu-block">
		<div class="side-menu-title-line">
			<img src="<?= ICONS ?>menu-trigger.png" alt="open-menu">
			<span class="side-title">כל המוצרים שלנו</span>
		</div>
		<ul class="side-menu-wrap">
			<?php if ($cats = opt('sidebar_cats')) : foreach ($cats as $cat_parent) :
				$children = get_terms( 'product_cat', [
						'parent'    => $cat_parent->term_id,
						'hide_empty' => false,
				]); ?>
				<li class="side-menu-item <?= $children ? 'parent-side-menu-item' : ''; ?>">
					<a class="cat-side-link cat-parent" href="<?= get_term_link($cat_parent); ?>">
						<?= $cat_parent->name; ?>
					</a>
					<?php if ($children) : ?>
						<span class="more-menu"></span>
						<ul class="sub-side-menu">
							<?php foreach ($children as $child) : ?>
								<li class="side-menu-item">
									<a class="cat-side-link" href="<?= get_term_link($cat_parent); ?>">
										<?= $cat_parent->name; ?>
									</a>
								</li>
							<?php endforeach; ?>
						</ul>
					<?php endif; ?>
				</li>
			<?php endforeach; endif; ?>
		</ul>
	</div>
</section>

