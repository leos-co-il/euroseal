<?php

the_post();
get_header();
$fields = get_fields();
$postId = get_the_ID();
$post_terms = wp_get_object_terms($postId, 'category', ['fields' => 'ids']);
$samePosts = [];
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} else {
	$samePosts = get_posts([
			'posts_per_page' => 4,
			'post_type' => 'post',
			'post__not_in' => array($postId),
			'tax_query' => [
					[
							'taxonomy' => 'category',
							'field' => 'term_id',
							'terms' => $post_terms,
					],
			],
	]);
}
if ($samePosts === NULL) {
	$samePosts = get_posts([
			'posts_per_page' => 4,
			'orderby' => 'rand',
			'post_type' => 'post',
			'post__not_in' => array($postId),
	]);
}
?>

<article class="page-body">
	<?php get_template_part('views/partials/content', 'block_top', [
		'title' => get_the_title(),
		'back_img' => has_post_thumbnail() ? postThumb() : '',
	]); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-10 col-md-11 col-12">
				<?php if ( function_exists('yoast_breadcrumb') ) : ?>
					<div class="row justify-content-start">
						<div class="col-12 breadcol">
							<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
		<div class="row justify-content-center align-items-start my-5">
			<div class="col-lg col-12">
				<div class="block-text block-text-single">
					<?php the_content(); ?>
				</div>
			</div>
			<?php if (has_post_thumbnail() || $fields['post_gallery']) : ?>
				<div class="col-lg-4 col-12 post-images-col">
					<?php if (has_post_thumbnail()) : ?>
						<img src="<?= postThumb(); ?>">
					<?php endif;
					if ($fields['post_gallery']) : foreach ($fields['post_gallery'] as $img) : ?>
						<img src="<?= $img['url']; ?>" alt="post-image">
					<?php endforeach; endif; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<?php if ($fields['single_step']) : ?>
		<div class="steps-block">
			<div class="container">
				<?php if ($fields['step_block_title']) : ?>
					<div class="row justify-content-center">
						<div class="col">
							<h2 class="base-title-blue text-center mb-3">
								<?= $fields['step_block_title']; ?>
							</h2>
						</div>
					</div>
				<?php endif; ?>
				<div class="row justify-content-center">
					<?php foreach ($fields['single_step'] as $key => $step) : ?>
						<div class="col-lg-4 col-md-6 col-12 col-after-line">
							<div class="step-item wow fadeIn" data-wow-delay="0.<?= $key; ?>s">
								<div class="step-num-wrap">
									<h2 class="step-num-title">
										<?= $key + 1; ?>
									</h2>
								</div>
								<p class="base-text text-center">
									<?= $step['step_desc']; ?>
								</p>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>



<?php get_template_part('views/partials/repeat', 'form');
if ($samePosts) : ?>
	<div class="block-part">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col">
					<h2 class="base-title-orange text-center mb-4">
						<?= $fields['same_title'] ? $fields['same_title'] : 'מאמרים נוספים'; ?>
					</h2>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($samePosts as $x => $post) : ?>
					<div class="col-xl-3 col-sm-6 col-12 post-col wow fadeInUp" data-wow-delay="0.<?= $x * 2; ?>s">
						<?php get_template_part('views/partials/card', 'post', [
								'post' => $post,
						]); ?>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>
